package com.tdd;

import com.tdd.Expression;

public class Money implements Expression {

    protected String currency;
    protected int amount;
  
    public Money(int amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }
    
    @Override
    public boolean equals(Object object) {
        Money money = (Money) object;
        return amount == money.getAmount() && getCurrency().equals(money.getCurrency());
    }
    
    // El cliente no conoce a las subclases pero la superclase sigue conociendo a sus hijos.
    // Esto está bien? o alguien más debería conocerlos?
    public static Money dollar(int amount) {
        return new Money(amount, "USD");
    }
    
    public static Money franc(int amount) {
        return new Money(amount, "CHF");
    }

    public Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }

    public String getCurrency() {
        return currency;
    }
    
    public int getAmount() {
        return amount;
    }
    
    public Expression plus(Money addend) {
        return new Money(amount + addend.amount, currency);
    }
    
}
